package model;

import java.util.ArrayList;

/**
 * @author Jonathan Burton
 */
public class Story {

    private String storyID;
    private ArrayList<Task> subTasks;

    public Story(String ID) {
        storyID = ID;
        subTasks = new ArrayList<>();
    }

    public String getStoryID() {
        return storyID;
    }

    public ArrayList<Task> getSubTasks() {
        return subTasks;
    }

    public void addSubTask(Task newTask) {
        subTasks.add(newTask);
    }


}
