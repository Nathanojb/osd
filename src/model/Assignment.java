package model;

/**
 * @author Jonathan Burton
 */
public class Assignment {

    private int cost;
    private Task task;
    private Staff staff;

    public Assignment(Task task, Staff staff) {
        this.task = task;
        this.staff = staff;

        cost = task.getDuration() * staff.getCostPerDay();
    }

    public int getCost() {
        return cost;
    }

    public Task getTask() {
        return task;
    }

    public Staff getStaff() {
        return staff;
    }
}
