package model;

import java.util.ArrayList;

/**
 * @author Jonathan Burton
 */
public class Task {

    private String taskID;
    private int duration;
    private ArrayList<Task> dependsOn;

    private ArrayList<Skill> requiredSkills;

    public Task(String ID, int taskDuration) {
        taskID = ID;
        duration = taskDuration;
        dependsOn = new ArrayList<>();
        requiredSkills = new ArrayList<>();
    }

    public String getTaskID() {
        return taskID;
    }

    public int getDuration() {
        return duration;
    }

    public ArrayList<Task> getDependsOn() {
        return dependsOn;
    }

    public ArrayList<Skill> getRequiredSkills() {
        return requiredSkills;
    }

    public void addDependency(Task dependency) {
        dependsOn.add(dependency);
    }

    public void addRequiredSkill(Skill skill) {
        requiredSkills.add(skill);
    }






}
