package model;

import java.util.ArrayList;

/**
 * @author Jonathan Burton
 */
public class Staff {

    private String staffID;
    private int costPerDay;
    private ArrayList<Skill> skills;

    public Staff(String ID, int cost) {
        staffID = ID;
        costPerDay = cost;
        skills = new ArrayList<>();
    }

    public String getStaffID() {
        return staffID;
    }

    public int getCostPerDay() {
        return costPerDay;
    }

    public ArrayList<Skill> getSkills() {
        return skills;
    }

    public void addSkill(Skill skill) {
        skills.add(skill);
    }

}
