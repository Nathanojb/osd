package model;

/**
 * @author Jonathan Burton
 */
public class Skill {

    private String skillID;

    public Skill(String ID) {
        skillID = ID;
    }

    public String getSkillID() {
        return skillID;
    }

}
