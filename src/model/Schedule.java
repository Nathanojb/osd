package model;

import java.util.ArrayList;

/**
 * @author Jonathan Burton
 */
public class Schedule {

    private int totalCost;
    private int totalDuration;
    private ArrayList<Assignment> assignments;

    public Schedule() {
        totalCost = 0;
        totalDuration = 0;
        assignments = new ArrayList<>();
    }

    public int getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(int totalCost) {
        this.totalCost = totalCost;
    }

    public int getTotalDuration() {
        return totalDuration;
    }

    public void setTotalDuration(int totalDuration) {
        this.totalDuration = totalDuration;
    }

    public ArrayList<Assignment> getAssignments() {
        return assignments;
    }

    public void addAssignment(Assignment assignment) {
        assignments.add(assignment);
    }
}
